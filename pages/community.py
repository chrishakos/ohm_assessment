from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user, confirm_login
from pdb import set_trace as bp
import json

from functions import app
from models import User

@app.route('/community', methods=['GET'])
def recent_users():
    if not confirm_login():
        login_user(User.query.get(1))
    users = User.get_recent_users()
    return render_template("community.html", users = users)
