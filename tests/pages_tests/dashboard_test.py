
from app_main import app
from tests import OhmTestCase
from flask.ext.login import login_user


class DashboardTest(OhmTestCase):
    def test_get(self):
        with app.test_client() as c:
            dashboard = c.get('/dashboard')
            assert "Ready to begin assessment" in dashboard.data

    def test_community(self):
        with app.test_client() as c:
            community = c.get('/community')
            assert self.newest_user.display_name in community.data
