from tests import OhmTestCase
from models import *


class UserTest(OhmTestCase):
    def test_get_multi(self):
        assert self.chuck.get_multi("PHONE") == ['+14086441234', '+14086445678']
        assert self.justin.get_multi("PHONE") == []

    def test_get_recent_users(self):
        users = User.get_recent_users()
        assert users[0].display_name == self.newest_user.display_name
        assert len(users) > 0
        assert len(users) <= 5
